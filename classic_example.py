from os import environ
from jps_api_wrapper.classic import Classic

JPS_URL = "https://example.jamfcloud.com"
JPS_USERNAME = environ["JPS_USERNAME"]
JPS_PASSWORD = environ["JPS_PASSWORD"]

with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic:
    mobile_devices = classic.get_mobile_devices()["mobile_devices"]
    ipad_116_ids = []
    for mobile_device in mobile_devices:
        if mobile_device["model_identifier"] == "iPad11,6":
            ipad_116_ids.append(mobile_device["id"])
    classic.create_mobile_device_command(
        "ScheduleOSUpdate", ipad_116_ids, install_action=2
    )
    print(f"Update command sent to devices {ipad_116_ids}.")
