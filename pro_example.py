from os import environ
from jps_api_wrapper.pro import Pro

JPS_URL = "https://example.jamfcloud.com"
JPS_USERNAME = environ["JPS_USERNAME"]
JPS_PASSWORD = environ["JPS_PASSWORD"]

with Pro(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as pro:
    mobile_devices = pro.get_mobile_devices()["results"]
    for mobile_device in mobile_devices:
        mobile_device_name = {
            "name": f"{mobile_device['username']}-{mobile_device['serialNumber']}"
        }
        pro.update_mobile_device(mobile_device_name, mobile_device["id"])
        print(f"Changed {mobile_device['name']} to {mobile_device_name['name']}")
